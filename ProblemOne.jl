### A Pluto.jl notebook ###
# v0.19.0

using Markdown
using InteractiveUtils

# ╔═╡ 5783d23a-38a2-4437-bfb3-653cdf912c38
using Pkg; Pkg.add("PlutoUI"); using PlutoUI

# ╔═╡ 7c5fc2f0-80bd-4dd9-a80a-b84c4702e8c6
begin
	Pkg.add("DataStructures")
	using DataStructures
end


# ╔═╡ 751a5201-7d3b-4978-a04a-0b840ca85258
begin
	using Markdown
	using InteractiveUtils
end


# ╔═╡ 0719e55e-9d62-4650-8fdb-33acc03a898d
using Markdown
using InteractiveUtils

# ╔═╡ 0ff3c521-a95f-4d7a-83fa-cc733b5b354b
md"# Defining Of Structures  "

# ╔═╡ f107a800-539c-4f2a-adee-ac8a573464af
struct State
	name::String
	agent::Location
	position::Int64
	parcel::vector{Int}
end

# ╔═╡ 09c23062-0559-409d-b2c9-b9ce96fae085
md"# Declaring Of Actions "

# ╔═╡ c1cc2275-a7c5-435a-99ae-a38c53d67ea4
begin
	moveEast = Action("moveEast", 2)

	moveWest = Action("moveWest", 2)

	moveUp= Action("moveUp",1)
	
	moveDown =Action("moveDown", 1)

	collect = Action("collect", 5)
end


# ╔═╡ f7a20487-7489-41a6-ac3d-2045c558f585
md"# Assigning Of States"

# ╔═╡ b8d33072-9b0e-4ba4-ac41-f42f44580d00
begin
	State1 = State("Num1", [1, 3, 2, 1], 1)
	
	State2 = State("Num2", [1, 3, 2, 1], 2)
	
	State3 = State("Num3", [1, 3, 2, 1], 3)
	
	State4 = State("Num4", [1, 3, 2, 1], 4)
	
	State5 = State("Num5", [0, 3, 2, 1], 1)
	
	State6 = State("Num6", [0, 3, 2, 1], 2)
	
	State7 = State("Num7",  [0, 3, 2, 1], 2)
	
	State8 = State("Num8", [0, 3, 2, 1], 4)
	
	State9 = State("Num9",  [0, 3, 2, 0], 4)
	
	State10 = State("Num10", [0, 3, 2, 0], 1)
	
	State11 = State("Num11", [0, 3, 2, 0], 1)
	
	State12 = State("Num12",  [0, 3, 2, 0], 2)
	
	State13 = State("Num13",  [0, 3, 2, 0], 3)
	
	State14 = State("Num14",  [0, 3, 2, 0], 3)
	
	State15 = State("Num15",  [0, 3, 2, 0], 2)
end


# ╔═╡ 4b4426ee-0d27-4bce-9c56-fb3f82a89cc3
md"# My Transition Model"

# ╔═╡ 9f7b4148-51e1-4bcf-9b92-30ea951d94cf
Trans_model = Dict()

push!(Trans_model, State1 => [( (collect, State2), (moveWest, State5), (moveUp, State7)])

push!(Trans_model,  State2 => [(moveUp, State5),  (moveDown, State4), (moveEast, State3)])

push!(Trans_model,  State3 => [(moveEast, State1), (moveWest, State3), (collect, State9)])

push!(Trans_model,  State4 => [(collect, State5),  (moveWest, State4), (moveEast, State3)])


push!(Trans_model,  State5 => [(moveUp, State5),  (moveDown, State4), (moveEast, State3)])


push!(Trans_model,  State6 => [(moveUp, State5),  (moveDown, State4), (moveWest, State3)])


push!(Trans_model,  State7 => [(moveDown, State5),  (moveUp, State4), (moveWest, State3)])


push!(Trans_model, State8 => [(collect, State6),  (moveDown, State12), (moveUp, State5)])

push!(Trans_model,  State9 => [(collect, State6),  (moveDown, State12), (moveEast, State3)])

push!(Trans_model,  State10 => [(moveDown, State5),  (moveDown, State4), (moveEast, State3)])

push!(Trans_model,  State11 => [(collect, State10),  (moveDown, State15), (moveUp, State5)])

push!(Trans_model,  State12 => [(moveUp, State15),  (moveDown, State4), (moveEast, State13)])

push!(Trans_model,  State13 => [(moveUp, State8),  (moveDown, State4), (moveEast, State4)])

push!(Trans_model,  State14 => [(moveUp, State6),  (collect, State5), (moveWest, State8)])

push!(Trans_model,  State15 => [(moveUp, State4),  (moveDown, State2), (moveEast, State9)])






# ╔═╡ 3723d042-50a7-41ea-bc15-fde84520447d
md"# Search a* function"

# ╔═╡ cedddde4-801d-4209-ad64-cf47590e78b2
md"# Goal Testing, Adding, Removing"

# ╔═╡ 525d8398-660a-4130-b685-7856eac11187
function goal(currentState::State)
    return ! (currentState.parcel[1] || currentState.parcel[2])
end


# ╔═╡ 78f6a3a0-cead-47a9-abfd-e04d8aa610c7
function add(queue::Queue{State}, state::State, cost::Int64)
	enqueue!(queue, state)
	return queue
end


# ╔═╡ e957d5df-42d5-4c60-b3ed-02902147883a
function to_pqueue_ucs(queue::PriorityQueue{State, Int64}, state::State,
cost::Int64)
	enqueue!(queue, state, cost)
	return queue
end


# ╔═╡ 2b4ad4c2-42c7-4d8b-a171-ba5dcd3be213
function add_to_stack(stack::Stack{State}, state::State, cost::Int64)
    push!(stack, state)
    return stack
end

# ╔═╡ ec3fec1c-9779-4e49-a8b4-8c7633778464
function remove(queue::Queue{State})
	removed = dequeue!(queue)
	return (removed, queue)
end


# ╔═╡ 2fe81231-ca19-422d-a6c6-8585a0f74b6d
function from_pqueue_ucs(queue::PriorityQueue{State, Int64})
	removed = dequeue!(queue)
	return (removed, queue)
end



# ╔═╡ ef75eaa0-cd21-4c2e-a0cc-8632cd3f818a
function remove_from_stack(stack::Stack{State})
    removed = pop!(stack)
    return (removed, stack)
end


# ╔═╡ 48874615-d2d5-4a10-a318-3363742e4077
md"# Ancestor states Results"

# ╔═╡ 2ea7f1d5-a70a-426c-bfe4-98481f8aea16
function result(trans_model, ancestors, initialState, goal)
result = []
explorer = goal
	while !(explorer == initialState)
		current_ancestor_state = ancestors[explorer]
		relations = trans_model[current_ancestor_state]
		for transition in relations
			if transition[2] == explorer
				push!(result, transition[1])
				break
			else

				continue
			end
		end
		explorer = current_ancestor_state
	end
	return result
end


# ╔═╡ 50db934f-f1c6-4efe-8b67-41bf881ceb06
md"# Search Next States Results"

# ╔═╡ f8046e2b-9b01-4c15-ac43-8bb447e0c71f
function result2(trans_model, ancestors, initialState, next)
result = []
next = initial
explorer = next
	while !(explorer == initial)
		current_ancestor_state = ancestors[explorer]
		relations = trans_model[current_ancestor_state]
		for transition in relations
			if trans[2] == explorer
				push!(result, trans[1])
				next = trans[2]
				break
			else
				continue
			end
		end
		explorer = current_ancestor_state
	end
	return result, next
	
end


# ╔═╡ 61133e95-afbe-4708-a3ca-3ed8325c5538
function search(initial, trans, goal, candidates, add, remove)
	explored = []
	ancestors = Dict{State, State}()
	the_candidates = add(candidates, initial, 0)
	parent = initialState
		while true
			if isempty(the_candidates)
				return []
			else
				(t1, t2) = remove(the_candidates)
				current_state = t1
				the_candidates = t2
	
		#runnung state is being handled
		push!(explored, current_state)
		candidates = trans[current_state]			
			for i in candidates			  
				if !(i[2] in explored)
					push!(ancestors, i[2] => current_state)
				if (goal(i[2]))
					return result(trans, ancestors,
					initial, i[2])
				else					
					the_candidates = add(the_candidates,
					i[2], i[1].cost)
				end
			end
			   
			end
		end
	end
end


# ╔═╡ cf1941b4-d5fb-488d-a979-f0829484ce7c
function heuristic(currentState, goal)
    return distance(currentState[1] - goal[1]) + distance(cell[2] - goal[2])
end

# ╔═╡ 34fbe7e7-c109-46e5-931f-b84abba2946f
function AStar(Trans_model,initialState, goal)
	
    result = []
	frontier = Queue{State}()
	explored = []
	parents = Dict{State, State}()
	first_state = true
    enqueue!(frontier, initialState)
	parent = initialState
	
    while true
		if isempty(frontier)
			return []
		else
			currentState = dequeue!(frontier)
			push!(explored, currentState)
			candidates = Trans_model[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(parents, single_candidate[2] => currentState)
					if (single_candiadte[2] in goal_state)
						return get_result(Trans_model, parents,initialState, single_candidate[2])
					else
						enqueue!(frontier, single_candidate[2])
					end
				end
			end
		end
	end
end


# ╔═╡ 8f2095ec-783a-4219-b041-31ee4ae0b672
function evaluate(state,goal)
	while (goal(state)) == false
		Star(state, trans_model, goal_test, Queue{State}(), add_to_queue,
remove_from_queue)
	end
end

# ╔═╡ a6274be2-eeb5-428b-b21c-e2e957022b88


# ╔═╡ cf8c0c0f-0772-4460-b167-07ea4f2ffe4a


# ╔═╡ 8e8ffdf2-a2eb-4f99-bc2c-fbc5569aa389


# ╔═╡ a07db320-52e9-4bce-b8f5-c803904c94a6


# ╔═╡ 4d9e2203-c3b8-470d-a8e4-fe49c84888a8


# ╔═╡ 853f6e0e-7d7d-4b62-8335-d989b6406ca1


# ╔═╡ 8c23db03-8524-4bc3-ba63-e90796c84173


# ╔═╡ f2bbce22-b46f-46d4-b655-297320e64438


# ╔═╡ f6aad96c-8b29-43c1-9531-93f71eed9d18


# ╔═╡ fd0c8ae0-d49d-4fed-aa4c-f72c37b9ba8c


# ╔═╡ e6e7490e-d428-4df9-8ab1-118b98550e26


# ╔═╡ d8b4b52c-4da0-418b-bba1-01b4bed3b88e


# ╔═╡ 9498c3f9-38df-41fe-a5b2-ec61acdf91cd


# ╔═╡ 22746d1b-e226-429e-b837-462d12fc223f


# ╔═╡ 35e93f1d-68d9-47a5-9154-34d54cc804ac


# ╔═╡ 3155dba0-0edb-443d-b011-a08670a775d3
@enum Action mU mD mE mW co

# ╔═╡ 8f3b7248-6222-418a-bae0-99806af648d9
struct Action
	Name::String
	cost::Int64
end


# ╔═╡ Cell order:
# ╠═0719e55e-9d62-4650-8fdb-33acc03a898d
# ╠═5783d23a-38a2-4437-bfb3-653cdf912c38
# ╠═7c5fc2f0-80bd-4dd9-a80a-b84c4702e8c6
# ╠═751a5201-7d3b-4978-a04a-0b840ca85258
# ╠═3155dba0-0edb-443d-b011-a08670a775d3
# ╠═0ff3c521-a95f-4d7a-83fa-cc733b5b354b
# ╠═8f3b7248-6222-418a-bae0-99806af648d9
# ╠═f107a800-539c-4f2a-adee-ac8a573464af
# ╠═09c23062-0559-409d-b2c9-b9ce96fae085
# ╠═c1cc2275-a7c5-435a-99ae-a38c53d67ea4
# ╠═f7a20487-7489-41a6-ac3d-2045c558f585
# ╠═b8d33072-9b0e-4ba4-ac41-f42f44580d00
# ╠═4b4426ee-0d27-4bce-9c56-fb3f82a89cc3
# ╠═9f7b4148-51e1-4bcf-9b92-30ea951d94cf
# ╠═3723d042-50a7-41ea-bc15-fde84520447d
# ╠═cedddde4-801d-4209-ad64-cf47590e78b2
# ╠═525d8398-660a-4130-b685-7856eac11187
# ╠═78f6a3a0-cead-47a9-abfd-e04d8aa610c7
# ╠═e957d5df-42d5-4c60-b3ed-02902147883a
# ╠═2b4ad4c2-42c7-4d8b-a171-ba5dcd3be213
# ╠═ec3fec1c-9779-4e49-a8b4-8c7633778464
# ╠═2fe81231-ca19-422d-a6c6-8585a0f74b6d
# ╠═ef75eaa0-cd21-4c2e-a0cc-8632cd3f818a
# ╠═48874615-d2d5-4a10-a318-3363742e4077
# ╠═2ea7f1d5-a70a-426c-bfe4-98481f8aea16
# ╠═50db934f-f1c6-4efe-8b67-41bf881ceb06
# ╠═f8046e2b-9b01-4c15-ac43-8bb447e0c71f
# ╠═61133e95-afbe-4708-a3ca-3ed8325c5538
# ╠═cf1941b4-d5fb-488d-a979-f0829484ce7c
# ╠═34fbe7e7-c109-46e5-931f-b84abba2946f
# ╠═8f2095ec-783a-4219-b041-31ee4ae0b672
# ╠═a6274be2-eeb5-428b-b21c-e2e957022b88
# ╠═cf8c0c0f-0772-4460-b167-07ea4f2ffe4a
# ╠═8e8ffdf2-a2eb-4f99-bc2c-fbc5569aa389
# ╠═a07db320-52e9-4bce-b8f5-c803904c94a6
# ╠═4d9e2203-c3b8-470d-a8e4-fe49c84888a8
# ╠═853f6e0e-7d7d-4b62-8335-d989b6406ca1
# ╠═8c23db03-8524-4bc3-ba63-e90796c84173
# ╠═f2bbce22-b46f-46d4-b655-297320e64438
# ╠═f6aad96c-8b29-43c1-9531-93f71eed9d18
# ╠═fd0c8ae0-d49d-4fed-aa4c-f72c37b9ba8c
# ╠═e6e7490e-d428-4df9-8ab1-118b98550e26
# ╠═d8b4b52c-4da0-418b-bba1-01b4bed3b88e
# ╠═9498c3f9-38df-41fe-a5b2-ec61acdf91cd
# ╠═22746d1b-e226-429e-b837-462d12fc223f
# ╠═35e93f1d-68d9-47a5-9154-34d54cc804ac
